import * as smartdrive from '../ts/index';

import { expect, tap } from '@pushrocks/tapbundle';

let myLocalDriveList: smartdrive.SmartDrive;
let driveList: smartdrive.IDrive[];

tap.test('should create a new LocalDriveList', async () => {
  myLocalDriveList = new smartdrive.SmartDrive();
  expect(myLocalDriveList).to.be.instanceof(smartdrive.SmartDrive);
});

tap.test('should deliver a local drivelist', async () => {
  driveList = await myLocalDriveList.getLocalDriveList();
});

tap.test('drivelist should contain drives', async () => {
  console.log(driveList);
});

tap.test('drivelist should contain drives', async () => {});

tap.start();
