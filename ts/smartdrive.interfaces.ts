import * as plugins from './smartdrive.plugins';

export interface ILsBlkJson {
  blockdevices: {
    name: string;
    'maj:min': string;
    rm: boolean;
    size: number;
    ro: boolean;
    type: 'disk' | 'part';
    mountpoint: null;
    children: {
      name: string;
      'maj:min': string;
      rm: boolean;
      size: string;
      ro: boolean;
      type: 'disk' | 'part';
      mountpoint: string;
    }[];
  }[];
}

export interface IDrive extends plugins.drivelist.Drive {
  children: ILsBlkJson['blockdevices'][0]['children'];
}

export interface IMountpoint {
  path: string;
}
